//
//  ViewController.h
//  ImagurSample
//
//  Created by Mesbah Uddin on 3/29/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *galleryItemsCollection;
- (IBAction)sectionValueChanged:(id)sender;
- (IBAction)viralValueChnaged:(id)sender;

@end

