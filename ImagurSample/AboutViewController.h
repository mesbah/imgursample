//
//  AboutViewController.h
//  ImagurSample
//
//  Created by Mesbah Uddin on 4/1/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *appNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *appVersionLabel;
@property (weak, nonatomic) IBOutlet UILabel *appAuthorLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorEmailLabel;
- (IBAction)dismissViewController:(id)sender;

@end
