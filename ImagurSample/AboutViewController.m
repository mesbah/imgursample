//
//  AboutViewController.m
//  ImagurSample
//
//  Created by Mesbah Uddin on 4/1/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//

#import "AboutViewController.h"

@implementation AboutViewController

-(void)viewDidLoad {
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
    
    self.appNameLabel.text = [NSString stringWithFormat:@"App Name: %@", infoDictionary[(NSString *)kCFBundleNameKey]];
    self.appVersionLabel.text = [NSString stringWithFormat:@"App Version: %@", infoDictionary[(NSString*)kCFBundleVersionKey]];
    
    self.appAuthorLabel.text = @"Author: Mohammed Mesbah Uddin";
    self.authorEmailLabel.text = @"Email: mesbah.t@gmail.com";
    
}


- (IBAction)dismissViewController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
