//
//  Constants.h
//  ImagurSample
//
//  Created by Mesbah Uddin on 3/29/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Utility
#define isNull(object) (object==nil || [object isKindOfClass:[NSNull class]])
#define nullSafeString(string) !isNull(string)?string:@""
#define iOSVersion [[[UIDevice currentDevice] systemVersion] floatValue]

extern NSString *const kClientID;


#pragma mark - Notifications
extern NSString *const kGalleryUpdatedNotification;
extern NSString *const kHttpRequestFailedNotification;

#pragma mark - API URLs
extern NSString *const kImageBaseURL;
extern NSString *const kGalleryBaseURL;

#pragma mark - HTTP request params key
extern NSString *const kAuthorizationKey;


#pragma mark - other constants
extern NSString *const kImageLinkBase;
