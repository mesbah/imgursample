//
//  ImagurApi.h
//  ImagurSample
//
//  Created by Mesbah Uddin on 3/29/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//

@interface ImagurAPI : NSObject
{
    AFHTTPRequestOperationManager *manager;
}

+ (ImagurAPI *)sharedInstance;
- (instancetype)init;

- (void)getGallery;
- (void)getImageWithID:(NSString *)imageID completionBlock:(void(^)(NSString *, NSString *)) completionBlock;
-(void)cancelAllRequest;
@end
