//
//  ImagurApi.m
//  ImagurSample
//
//  Created by Mesbah Uddin on 3/29/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//

#import "ImagurAPI.h"
#import "GalleryObject.h"

@implementation ImagurAPI

- (instancetype)init
{
    if (self = [super init])
    {
        manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}

+ (ImagurAPI*)sharedInstance
{
    static ImagurAPI *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[ImagurAPI alloc] init];
    });
    return _sharedInstance;
}

void(^genericFailureBlock)(AFHTTPRequestOperation *, NSError *) = ^(AFHTTPRequestOperation *operation, NSError *error) {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kHttpRequestFailedNotification object:nil];
    
    [[GlobalHelper sharedInstance] hideLoading];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
};

void (^genericSuccessBlock)(AFHTTPRequestOperation *, id) = ^(AFHTTPRequestOperation *operation, id responseObject) {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [[GlobalHelper sharedInstance] hideLoading];
};


-(void) makeRequest:(NSString *) url
            success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
            failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    [manager.requestSerializer setValue:kClientID forHTTPHeaderField:kAuthorizationKey];
    
    [manager GET:url parameters:nil success:success failure:failure];
}

- (void) getGallery
{
    void (^successBlock)(AFHTTPRequestOperation *, id) = ^(AFHTTPRequestOperation *operation, id responseObject) {
        [[GlobalHelper sharedInstance] hideLoading];
        genericSuccessBlock(operation, responseObject);
        
        [[[GlobalDataStore sharedInstance] galleryObjectKeyArray] removeAllObjects];
        //[[[GlobalDataStore sharedInstance] galleryObjectDictionary] removeAllObjects];

        for (NSDictionary *galleryObjectDictionary in responseObject[@"data"]) {
            GalleryObject *galleryObject = [[GalleryObject alloc] initWithDictionary:galleryObjectDictionary];
            
            [[[GlobalDataStore sharedInstance] galleryObjectKeyArray] addObject:galleryObject.imageID];
            
            if ([[[GlobalDataStore sharedInstance] galleryObjectDictionary] objectForKey:galleryObject.imageID]==nil) {
                [[[GlobalDataStore sharedInstance] galleryObjectDictionary] setObject:galleryObject forKey:galleryObject.imageID];
            }
        }
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kGalleryUpdatedNotification object:nil];
        
        [[GlobalHelper sharedInstance] showToast:@"Success"];
    };
    
    void(^failureBlock)(AFHTTPRequestOperation *, NSError *) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [[GlobalHelper sharedInstance] hideLoading];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        genericFailureBlock(operation, error);
        
        [[GlobalHelper sharedInstance] showToast:@"Failed"];

    };
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    GlobalDataStore *globalStore = [GlobalDataStore sharedInstance];
    
    NSString *url;
    url = [NSString stringWithFormat:@"%@?showViral=%@&_format=json",kGalleryBaseURL,globalStore.isViral];
    if (globalStore.section.length>0) {
        url = [NSString stringWithFormat:@"%@%@/?showViral=%@&_format=json",kGalleryBaseURL,globalStore.section,globalStore.isViral];
    }
    
    [self makeRequest:url success:successBlock failure:failureBlock];
    [[GlobalHelper sharedInstance] showLoading];
}

- (void)getImageWithID:(NSString *)imageID completionBlock:(void(^)(NSString *, NSString *)) completionBlock
{
    void (^successBlock)(AFHTTPRequestOperation *, id) = ^(AFHTTPRequestOperation *operation, id responseObject) {
        [[GlobalHelper sharedInstance] hideLoading];
        genericSuccessBlock(operation, responseObject);
        
        completionBlock(responseObject[@"data"][@"link"],responseObject[@"data"][@"id"]);

    };
    
    void(^failureBlock)(AFHTTPRequestOperation *, NSError *) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [[GlobalHelper sharedInstance] hideLoading];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        genericFailureBlock(operation, error);
    };
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kImageBaseURL,imageID];
    
    [self makeRequest:url success:successBlock failure:failureBlock];
    
}

-(void)cancelAllRequest
{
    for(AFHTTPRequestOperation *op in [manager.operationQueue operations]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [[GlobalHelper sharedInstance] hideLoading];
            
            [op cancel];
    }
}


@end
