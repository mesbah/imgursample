//
//  GalleryObject.h
//  ImagurSample
//
//  Created by Mesbah Uddin on 3/31/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GalleryObject : NSObject
@property (nonatomic, strong) NSString *imageID;
@property (nonatomic, strong) NSString *imageTitle;
@property (nonatomic, strong) NSString *imageDescription;

@property BOOL isAlbum;
@property BOOL isVisited;

@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *imageThumbLink;

- (instancetype) initWithDictionary:(NSDictionary *)dictionary;
- (void) loadImage;

@end
