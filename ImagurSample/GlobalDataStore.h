//
//  GlobalDataStore.h
//  ImagurSample
//
//  Created by Mesbah Uddin on 3/29/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//


@interface GlobalDataStore : NSObject

@property (nonatomic, strong) NSString *isViral;
@property (nonatomic, strong) NSString *section;

@property (nonatomic, strong) NSMutableArray *galleryObjectKeyArray;
@property (nonatomic, strong) NSMutableDictionary *galleryObjectDictionary;


+ (GlobalDataStore *)sharedInstance;
- (instancetype)init;

@end
