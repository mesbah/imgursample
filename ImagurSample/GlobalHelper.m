//
//  GlobalHelper.m
//  ImagurSample
//
//  Created by Mesbah Uddin on 3/29/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//

#import "GlobalHelper.h"

#import <MBProgressHUD.h>
#import "AppDelegate.h"

@interface GlobalHelper ()
{
    AppDelegate *appDelegate;
    MBProgressHUD *globalHud;
}
@end

@implementation GlobalHelper

-(instancetype)init
{
    if (self = [super init])
    {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    
    return self;
}

+ (GlobalHelper*)sharedInstance
{
    static GlobalHelper *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[GlobalHelper alloc] init];
    });
    return _sharedInstance;
}

-(void) showLoading
{
    globalHud = [MBProgressHUD showHUDAddedTo:appDelegate.window animated:YES];
}

-(void) hideLoading
{
    [globalHud hide:YES];
}

-(void) showToast:(NSString *) message
{
    [self hideLoading];
    globalHud = [MBProgressHUD showHUDAddedTo:appDelegate.window animated:YES];
    globalHud.mode = MBProgressHUDModeText;
    [globalHud setDetailsLabelText:message];
    
    [self performSelector:@selector(hideLoading) withObject:nil afterDelay:2];
}

-(CGRect) screenSize
{
    CGRect cScreenSize =[[UIScreen mainScreen] bounds];
    
    if (iOSVersion<8.0) {
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        
        if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
        {
            CGFloat currentHeight = cScreenSize.size.height;
            cScreenSize.size.height = cScreenSize.size.width;
            cScreenSize.size.width = currentHeight;
        }
    }
    
    return cScreenSize;
}

-(int)getDeviceOrientation
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    if(orientation == 0) //Default orientation
        return 0;
    else if(orientation == UIInterfaceOrientationPortrait)
        return 1;
    else if(orientation == UIInterfaceOrientationLandscapeLeft)
        return 2;
    else if(orientation == UIInterfaceOrientationLandscapeRight)
        return 3;
    return 4;
}

-(NSString *)getThumbLinkFromURL:(NSString *)fullLink withImageID:(NSString *)imageID {
    NSString *imageNameWithExtention = [fullLink stringByReplacingOccurrencesOfString:kImageLinkBase withString:@""];
    
    NSString *imageExtensionOnly = [imageNameWithExtention componentsSeparatedByString:@"."][1];
    
    NSString *thumbLink = [NSString stringWithFormat:@"%@%@s.%@",kImageLinkBase, imageID, imageExtensionOnly];
    
    return thumbLink;
}

@end
