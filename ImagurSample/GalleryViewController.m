//
//  ViewController.m
//  ImagurSample
//
//  Created by Mesbah Uddin on 3/29/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//

#import "GalleryViewController.h"
#import "GalleryObject.h"

@interface GalleryViewController ()

@end

@implementation GalleryViewController

- (void) galleryListReceived
{
    [self.galleryItemsCollection reloadData];
}

- (void)imageLoaded:(NSNotification *)notification
{
    NSURL *URL = (notification.userInfo)[AsyncImageURLKey];

    UIImage *image = (notification.userInfo)[AsyncImageImageKey];
    
    NSArray * visibleIndexPaths = [self.galleryItemsCollection indexPathsForVisibleItems];
    
    for (NSIndexPath *cIndexPath in visibleIndexPaths) {
        GalleryObject *currentObject = [[GlobalDataStore sharedInstance] galleryObjectDictionary][[[GlobalDataStore sharedInstance] galleryObjectKeyArray][cIndexPath.row]];
        if ([currentObject.imageThumbLink isEqualToString:[URL absoluteString]]) {
            UICollectionViewCell *cell = [self.galleryItemsCollection cellForItemAtIndexPath:cIndexPath];
            
            AsyncImageView *thumbImageView = (AsyncImageView *)[cell viewWithTag:102];
            UIActivityIndicatorView *activityIndicatorView = (UIActivityIndicatorView *)[cell viewWithTag:1112];
            
            
            thumbImageView.image = nil;
            activityIndicatorView.hidden=NO;
            thumbImageView.image = image;
            activityIndicatorView.hidden=YES;
            break;
        }
    }
    
    
}

- (void)imageFailed:(NSNotification *)notification
{
    NSURL *URL = (notification.userInfo)[AsyncImageURLKey];
    
    [[AsyncImageLoader sharedLoader] loadImageWithURL:URL];
}

-(void)refreshThumbs
{
    for (NSString *currentGalleryObjectKey in [[GlobalDataStore sharedInstance] galleryObjectKeyArray]) {
        GalleryObject *currentGalleryObject = (GalleryObject *)[[GlobalDataStore sharedInstance] galleryObjectDictionary][currentGalleryObjectKey];
        if (currentGalleryObject.isVisited) {
            [currentGalleryObject loadImage];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[ImagurAPI sharedInstance] getGallery];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(galleryListReceived) name:kGalleryUpdatedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(imageLoaded:)
                                                 name:@"AsyncImageLoadDidFinish" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(imageFailed:)
                                                 name:@"AsyncImageLoadDidFail" object:nil];
    
    [NSTimer scheduledTimerWithTimeInterval:7.0 target:self selector:@selector(refreshThumbs) userInfo:nil repeats:YES];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGalleryUpdatedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AsyncImageLoadDidFinish" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AsyncImageLoadDidFail" object:nil];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[GlobalDataStore sharedInstance] galleryObjectKeyArray].count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GalleryThumbIdentifier" forIndexPath:indexPath];
    UILabel *descriptionlabel = (UILabel *)[cell viewWithTag:101];
    AsyncImageView *thumbImageView = (AsyncImageView *)[cell viewWithTag:102];
    UIActivityIndicatorView *activityIndicatorView = (UIActivityIndicatorView *)[cell viewWithTag:1112];
    
    
    [thumbImageView setCrossfadeDuration:0];
    GalleryObject *currentGalleryObject = [[GlobalDataStore sharedInstance] galleryObjectDictionary][[[GlobalDataStore sharedInstance] galleryObjectKeyArray][indexPath.row]];
    currentGalleryObject.isVisited = YES;

    descriptionlabel.text = currentGalleryObject.imageTitle.length>0?currentGalleryObject.imageTitle:currentGalleryObject.imageDescription;
    
    thumbImageView.image = nil;
    activityIndicatorView.hidden=NO;

    UIImage *image = [[AsyncImageLoader defaultCache] objectForKey:[NSURL URLWithString:currentGalleryObject.imageThumbLink]];
    if (image) {
        thumbImageView.image = image;
        activityIndicatorView.hidden=YES;

    } else {
        if (currentGalleryObject.isAlbum) {
            if (currentGalleryObject.imageThumbLink) {
                [currentGalleryObject loadImage];
            }
        } else {
            [currentGalleryObject loadImage];
        }
    }
    

    return cell;
}


- (IBAction)sectionValueChanged:(id)sender {
    UISegmentedControl *sectionSegmentedControl = (UISegmentedControl *)sender;
    
    switch (sectionSegmentedControl.selectedSegmentIndex) {
        case 0:
            [[GlobalDataStore sharedInstance] setSection:@""];
            break;
        case 1:
            [[GlobalDataStore sharedInstance] setSection:@"hot"];
            break;
        case 2:
            [[GlobalDataStore sharedInstance] setSection:@"top"];
            break;
        case 3:
            [[GlobalDataStore sharedInstance] setSection:@"user"];
            break;
            
        default:
            break;
    }
    
    [[[GlobalDataStore sharedInstance] galleryObjectKeyArray] removeAllObjects];
    [self.galleryItemsCollection reloadData];
    
    [[ImagurAPI sharedInstance] cancelAllRequest];
    [[ImagurAPI sharedInstance] getGallery];
}

- (IBAction)viralValueChnaged:(id)sender {
    UISegmentedControl *sectionSegmentedControl = (UISegmentedControl *)sender;
    
    switch (sectionSegmentedControl.selectedSegmentIndex) {
        case 0:
            [[GlobalDataStore sharedInstance] setIsViral:@"false"];
            break;
        case 1:
            [[GlobalDataStore sharedInstance] setIsViral:@"true"];
            break;
        default:
            break;
    }
    [[[GlobalDataStore sharedInstance] galleryObjectKeyArray] removeAllObjects];
    [self.galleryItemsCollection reloadData];
    [[ImagurAPI sharedInstance] cancelAllRequest];
    [[ImagurAPI sharedInstance] getGallery];
}
@end
