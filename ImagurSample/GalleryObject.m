//
//  GalleryObject.m
//  ImagurSample
//
//  Created by Mesbah Uddin on 3/31/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//


#import "GalleryObject.h"

@implementation GalleryObject

- (instancetype) initWithDictionary:(NSDictionary *)dictionary
{
    if (self = [super init])
    {
        _imageID = nullSafeString(dictionary[@"id"]);
        _imageTitle = nullSafeString(dictionary[@"title"]);
        _imageDescription = nullSafeString(dictionary[@"description"]);
        
        _isAlbum = [dictionary[@"is_album"] boolValue];
        
        _link = dictionary[@"link"];
        
        if (_isAlbum) {
            void (^imageLinkReceived)(NSString *, NSString *) = ^(NSString *imageLinkP, NSString *imageIDP){
                _imageThumbLink = [[GlobalHelper sharedInstance] getThumbLinkFromURL:imageLinkP withImageID:imageIDP];
                
                if (_isVisited) {
                    [self loadImage];
                }
                
            };
            
            [[ImagurAPI sharedInstance] getImageWithID:dictionary[@"cover"] completionBlock:imageLinkReceived];
            
        } else {
            _imageThumbLink = [[GlobalHelper sharedInstance] getThumbLinkFromURL:_link withImageID:_imageID];
        }
        
        _isVisited = NO;
    }
    
    return self;
}

- (void) loadImage {
    if ([[AsyncImageLoader defaultCache] objectForKey:[NSURL URLWithString:_imageThumbLink]]==nil) {
        [[AsyncImageLoader sharedLoader] loadImageWithURL:[NSURL URLWithString:_imageThumbLink]];
    }
}

@end
