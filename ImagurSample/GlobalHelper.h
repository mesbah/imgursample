//
//  GlobalHelper.h
//  ImagurSample
//
//  Created by Mesbah Uddin on 3/29/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//

@interface GlobalHelper : NSObject

+ (GlobalHelper*)sharedInstance;
-(instancetype)init;

-(void) showLoading;
-(void) hideLoading;
-(void) showToast:(NSString *) message;

-(CGRect) screenSize;
-(int) getDeviceOrientation;

-(NSString *)getThumbLinkFromURL:(NSString *)fullLink withImageID:(NSString *)imageID;

@end
