//
//  GlobalDataStore.m
//  ImagurSample
//
//  Created by Mesbah Uddin on 3/29/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//

#import "GlobalDataStore.h"

@implementation GlobalDataStore

- (instancetype)init
{
    if (self = [super init])
    {
        _galleryObjectKeyArray = [[NSMutableArray alloc] init];
        _galleryObjectDictionary = [[NSMutableDictionary alloc] init];
        _isViral = @"false";
        _section = @""; //hot, top,user
    }
    
    return self;
}

+ (GlobalDataStore*)sharedInstance
{
    static GlobalDataStore *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[GlobalDataStore alloc] init];
    });
    return _sharedInstance;
}

@end
