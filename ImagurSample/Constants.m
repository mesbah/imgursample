//
//  Constants.m
//  ImagurSample
//
//  Created by Mesbah Uddin on 3/29/15.
//  Copyright (c) 2015 Personal Project. All rights reserved.
//

#import "Constants.h"


NSString *const kClientID = @"Client-ID eb4dbd24b74e5aa";


#pragma mark - Notifications
NSString *const kGalleryUpdatedNotification = @"com.imagurSample.galleryUpdated";
NSString *const kHttpRequestFailedNotification = @"com.imagurSample.httpRequestFailedNotification";

#pragma mark - API URLs

//https://api.imgur.com/3/gallery/{section}/{sort}/{page}?showViral=bool

//section hot, top, user,
//include / exclude viral images

NSString *const kGalleryBaseURL = @"https://api.imgur.com/3/gallery/";
NSString *const kImageBaseURL = @"https://api.imgur.com/3/image/";

#pragma mark - HTTP request params key
NSString *const kAuthorizationKey = @"Authorization";

#pragma mark - other constants
NSString *const kImageLinkBase = @"http://i.imgur.com/";